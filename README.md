# Task 1.2 - Kubernetes



## 1. Установите minikube согласно инструкции на официальном сайте.
Установка

#предполагаем что docker уже установлен (с прошлого задания)
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```
Также установим kubectl для управления
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
kubectl version --client
```
Запуск
```
minikube config set driver docker
minikube start # --vm-driver=docker
minikube status
```
![kubernetes1](kubernetes1.png)

## 2.Создайте namespace для деплоя простого веб приложения.
helloworld-namespace.yaml
```
apiVersion: v1
kind: Namespace
metadata:
  name: helloworld
```
Создание
```
kubectl create -f ./helloworld-namespace.yaml
#namespace/helloworld created
```

## 3. Напишите deployments файл для установки в Kubernetes простого веб приложения

helloworld-deployment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-deployment
spec:
  selector:
    matchLabels:
      app: hello-world
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
      - name: hello-world
        image: crccheck/hello-world
        ports:
        - containerPort: 8000
```
Применение
```
kubectl apply -f ./helloworld-deployment.yaml -n helloworld
kubectl expose deployment hello-world-deployment -n helloworld
```
## 4. Установите в кластер ingress контроллер 
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/cloud/deploy.yaml

kubectl patch deployment ingress-nginx-controller --patch "$(cat ingress-nginx-controller-patch.yaml)" -n ingress-nginx
```
ingress-nginx-controller-patch.yaml
```
spec:
  template:
    spec:
      containers:
      - name: controller
        ports:
         - containerPort: 80
           hostPort: 8080
```
## 5. Напишите и установите Ingress rule для получения доступа к своему приложению.
```
#предварительно смотрим вывод minikube ip и в /etc/hosts вписываем полученный адрес для доменного имени helloworld.local
kubectl create ingress helloworld-localhost -n helloworld --class=nginx --rule=helloworld.local/=hello-world-deployment:8000
```
В качестве результата работы сделайте скриншоты результата выполнения команд:

kubectl get pods -A

kubectl get svc

kubectl get all

![kubernetes2](kubernetes2.png)

а также все написанные вами файлы конфигурации